package mpr1_2;

public class Adres {
	private String ulica;
	private int nr_d;
	private int nr_m;
	public Adres(String ulica, int nr_d, int nr_m) {
		this.ulica = ulica;
		this.nr_d = nr_d;
		this.nr_m = nr_m;
	}
	public String getUlica() {
		return ulica;
	}
	public void setUlica(String ulica) {
		this.ulica = ulica;
	}
	public int getNr_d() {
		return nr_d;
	}
	public void setNr_d(int nr_d) {
		this.nr_d = nr_d;
	}
	public int getNr_m() {
		return nr_m;
	}
	public void setNr_m(int nr_m) {
		this.nr_m = nr_m;
	}
	public String podajAdres()
	{
		return getUlica()+" "+getNr_d()+"/"+getNr_m();
	}


	

}
