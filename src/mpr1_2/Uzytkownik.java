package mpr1_2;

public class Uzytkownik {
	private int login;
	private String haslo;
	public Uzytkownik(int login, String haslo) {
		this.login = login;
		this.haslo = haslo;
	}
	public int getLogin() {
		return login;
	}
	public void setLogin(int login) {
		this.login = login;
	}
	public String getHaslo() {
		return haslo;
	}
	public void setHaslo(String haslo) {
		this.haslo = haslo;
	}
	public int podajLogin()
	{
		return getLogin();
	}
	public String podajHaslo()
	{
		return getHaslo();
	}
	

}
